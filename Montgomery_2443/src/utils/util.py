import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# %matplotlib inline
import seaborn as sns


from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.decomposition import PCA

def summary_missing_data(df: pd.DataFrame, lowest_proportion: float = 0.0) -> pd.DataFrame:
    total = df.isnull().sum().sort_values(ascending=False)
    percent = (df.isnull().sum()/df.isnull().count()).sort_values(ascending=False)
    missing_data = pd.concat([total, percent], axis=1, keys=['Count', 'Percent'])
    return missing_data[missing_data['Percent'] > lowest_proportion]

def pearson_correlation_heatmap(df: pd.DataFrame, size=25) -> None:
    _ , ax = plt.subplots(figsize =(14, 12))
    colormap = sns.diverging_palette(220, 10, as_cmap = True)
    _ = sns.heatmap(
        df.corr(), 
        cmap = colormap,
        square=True, 
        cbar_kws={'shrink':.9 }, 
        ax=ax,
        annot=True, 
        linewidths=0.1,vmax=1.0, linecolor='white',
        annot_kws={'fontsize':12 }
    )
    plt.title('Pearson Correlation of Features', y=1.05, size=size)
    
def _eval_components(Z):
    for i in range(1, Z.shape[1], 1):
        pca = PCA(n_components=i).fit(Z)
        # print('Variance ratio = ', pca.explained_variance_ratio_)
        print('Cumulative sum:\t', sum(pca.explained_variance_ratio_), '\twith', pca.n_components_, 'components')
        
def evaluate_regressor(model, X, Y, name=None, nruns=200, other_metric=None):
    r2, mse, extra = [], [], []
    for j in range(nruns):
        xtrain, xtest, ytrain, ytest = train_test_split(X, Y)
        model.fit(xtrain, ytrain)
        YP = model.predict(xtest)
        r2.append(r2_score(YP, ytest))
        mse.append(mean_squared_error(YP, ytest))
        if other_metric!=None:
            keep_positives = YP >= 0
            extra.append(other_metric['call'](YP[keep_positives], ytest[keep_positives]))
    print("Runs:\t\t", nruns)
    print("Mean R2:\t", np.mean(r2), "\nSTD R2:\t\t", np.std(r2))
    print("Mean MSE:\t", np.mean(mse), "\nSTD MSE:\t", np.std(mse))
    if other_metric!=None: print(other_metric['name']+":\t\t", np.mean(extra))
    plt.hist(r2)
    plt.title("R2 Histogram - "+name)
    plt.xlim(0, 1)
    
def break_and_save_df(comb: pd.DataFrame, brk: int=None, fname: str='cl', Y: pd.Series=None, Ylabel: str=None) -> None:
    """
        Deals with either a combined DF with the training/test data when brk is given
        Or, saves an individual file without breaking
    """
    if brk is not None:
        traindata = comb.iloc[:brk,:]
        testdata = comb.iloc[brk:,:]
        if Y is not None and Ylabel is not None:
            traindata[Ylabel] = Y
            testdata.drop(columns=[Ylabel], inplace=True)
        traindata.to_csv('./data/'+fname+'_train.csv', index=False)
        testdata.to_csv('./data/'+fname+'_test.csv', index=False)
    else:
        comb.to_csv('./data/'+fname+'.csv', index=False)


def evaluate_regressor(model, X, Y, name=None, nruns=200, other_metric=None):
    r2, mse, extra = [], [], []
    for j in range(nruns):
        xtrain, xtest, ytrain, ytest = train_test_split(X, Y)
        model.fit(xtrain, ytrain)
        YP = model.predict(xtest)
        r2.append(r2_score(YP, ytest))
        mse.append(mean_squared_error(YP, ytest))
        if other_metric!=None:
            keep_positives = YP >= 0
            extra.append(other_metric['call'](YP[keep_positives], ytest[keep_positives]))
    print("Runs:\t\t", nruns)
    print("Mean R2:\t", np.mean(r2), "\nSTD R2:\t\t", np.std(r2))
    print("Mean MSE:\t", np.mean(mse), "\nSTD MSE:\t", np.std(mse))
    if other_metric!=None: print(other_metric['name']+":\t\t", np.mean(extra))
    plt.hist(r2)
    plt.title("R2 Histogram - "+name)
    plt.xlim(0, 1)
    
def plot_horizontal_hist(xplot, yplot, xlabel, title):
    fig, ax = plt.subplots(figsize=(20,10))
#     xplot = list(accdf['Precision'])
#     yplot = list(accdf['Classifier'])
    ax = sns.barplot(x=xplot, y=yplot, palette=sns.light_palette("yellow", n_colors=len(xplot), reverse=True))
    # plt.ylabel("")
    plt.xlabel(xlabel, fontsize=20)
    plt.title(title, fontsize=25)
    plt.gcf().set_size_inches(8,8)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(20) 
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    for p in ax.patches:
        width = p.get_width()
        ax.text(width, p.get_y() + p.get_height()/1.4, "%.2f" % width, ha="center", fontsize=15)
    sns.despine()
    plt.plot();
    
def show_grid_results(grid_search, all=True):
    """
        Prints scores and parameters from given gridsearch
    """
    print('Best parameters:\n', grid_search.best_params_, '\n', grid_search.best_score_, '\n')
    if all:
        cvres = grid_search.cv_results_
        for mean_score, params in zip(cvres["mean_test_score"], cvres["params"]):
            print(mean_score, params)