This file contains brief explanation of what each folder contains:

Archive/
    Old not-being-used files

AWARE Codebook/
    Files that explains the features of the dataset
    Key Variables chosen by psycology student Montgomery at in the Key Variables file

Data/
    Raw and compiled datasets used so far including the scores

Reports/
    Some write ups of the project

src/
    Source code used so far
    Code is on Jupyter Notebooks
    All notebooks are commented